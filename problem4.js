let problem4 = (array)=>
{    
    if(array.length==0)
    {
        return []
    }
    
    let emailArray=[]
   
    for(let index = 0; index<array.length; index++)
    {
    emailArray.push(array[index].email)
    }

    emailArray.sort()
    return emailArray;
}

module.exports = problem4;